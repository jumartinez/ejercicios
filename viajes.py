
def calcular_gastos(km, precio_gasolina_lt, gasto_gasolina, tiempo_viaje):
    cop_eu = 0.00028
    tiempo_viaje = tiempo_viaje.split(":")
    tiempo_viaje = float(tiempo_viaje[0])+round(float(tiempo_viaje[1])/60,2)
    # Consumo en 100km
    consumo_ga_lt = 100 * ((precio_gasolina_lt * cop_eu) / (gasto_gasolina * cop_eu))/km
    consumo_ga_eu = (100 * (gasto_gasolina * cop_eu) / km)


    # Velocidad media
    velocidad_km = km / tiempo_viaje
    velocidad_m = (km * 1000) / (tiempo_viaje*3600)
    print("Velocidad media")
    print("VM en Km/h: {} - VM en M/s {}".format(round(velocidad_km,2), round(velocidad_m, 2)))
    print("Consumo de gasolina en 100km")
    print("Litros: {} - Euros {}".format(round(consumo_ga_lt, 2),round(consumo_ga_eu),2))
    print("Consumo de gasolina en 1km")
    print("Litros: {} - Euros {}".format(consumo_ga_lt/100, consumo_ga_eu/100))


km = float(input("Ingrese el total de km recorridos: "))
precio_gasolina_lt = float(input("Ingrese el precio de la gasolina por litro en $COP: "))
gasto_gasolina = float(input("Ingrese el dinero gastado en gasolina: "))
tiempo_viaje = input("Ingrese el tiempo de viaje en total HH:MM: ")

calcular_gastos(km, precio_gasolina_lt, gasto_gasolina, tiempo_viaje)