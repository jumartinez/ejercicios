import pprint

# Todas las funciones para calcular valores de la nomina
def cesantias(sm, dt):
    return (sm*dt)/360

def i_cesantias(cesantias, dt):
    return (cesantias * dt * 0.12)/360

def p_servicio(sm,dt):
    return ((sm * 180))/360

def vacaciones(smb, dt):
    return (smb * dt)/720

def horas_ext_di(vho):
    return vho*1.25

def horas_nocturna(vho):
    return vho*1.35

def horas_ext_no(vho):
    return vho*1.75

def horas_odf(vho):
    return vho*1.75

def horas_eddf(vho):
    return vho*2

def horas_endf(vho):
    return vho*2.25

def valor_hora(sm):
    return sm/240

# Esta funcion pide los datos al usuario y calcula su nomina
def calcular_nomina():
    SALARIO_MIN = 850000
    AUX_T = 72250
    salir = ""
    total = 0
    # Lista de nomina por usuario
    lista_nomina = []
    while True:
        # Diccionario de nomina
        nomina = dict()

        # Datos de la nomina
        nombre = input("Ingrese su nombre completo: ")
        dias_trabajados = int(input("Ingrese el # de dias trabajados: "))
        salario_mensual = int(input("Ingrese el # de (sm): "))
        hed = int(input("Ingrese el # de horas extras diurnas: "))
        hn = int(input("Ingrese el # de horas nocturnas: "))
        hen = int(input("Ingrese el # de horas extras nocturnas: "))
        hodf = int(input("Ingrese el # de horas ordinarias dominicales o festiva: "))
        heddf = int(input("Ingrese el # de horas extras diurnas dominicales o festivas: "))
        hendf = int(input("Ingrese el # de horas extras nocturna dominical o festiva: "))

        nomina["name"] = nombre
        if salario_mensual * SALARIO_MIN < SALARIO_MIN * 2:
            nomina["SM"] = salario_mensual * SALARIO_MIN + AUX_T
        nomina["SM"] = salario_mensual * SALARIO_MIN
        nomina["HED"] = [hed,hed*horas_ext_di(valor_hora(nomina["SM"]))]
        nomina["HN"] = [hn,hn*horas_nocturna(valor_hora(nomina["SM"]))]
        nomina["HEN"] = [hen,hen*horas_ext_no(valor_hora(nomina["SM"]))]
        nomina["HODF"] = [hodf,hodf*horas_odf(valor_hora(nomina["SM"]))]
        nomina["HEDDF"] = [heddf,heddf*horas_eddf(valor_hora(nomina["SM"]))]
        nomina["HENDF"] = [hendf,hendf*horas_endf(valor_hora(nomina["SM"]))]
        nomina["Cesantias"] = cesantias(nomina["SM"],dias_trabajados)
        nomina["I. Sobre Cesantias"] = i_cesantias(nomina["Cesantias"],dias_trabajados)
        nomina["P. de Servicios"] = p_servicio(nomina["SM"],dias_trabajados)
        nomina["Vacaciones"] = vacaciones(SALARIO_MIN, dias_trabajados)
        nomina["Liquidacion"] = nomina["Cesantias"] + nomina["I. Sobre Cesantias"] + nomina["Vacaciones"] + nomina["P. de Servicios"]
        nomina["Neto Pagado"] = nomina["SM"] + nomina["Liquidacion"] + nomina["HED"][1] + nomina["HN"][1] + nomina["HEN"][1] + nomina["HODF"][1] + nomina["HEDDF"][1] + nomina["HENDF"][1]
        lista_nomina.append(nomina)
        salir = input("Quiere seguir registrando empleados? (si/no)")
        if salir.lower() != "si":
            break

    for i in lista_nomina:
        total += i["Neto Pagado"]

    print("Nomina de la empresa")
    pprint.pprint(lista_nomina)
    print("El total de la nomina de la empresa es de", round(total,2))

calcular_nomina()










