from yattag import Doc
import urllib.request as request
import json

# Imprime en tablas las listas pasadas por parametro
def imprimir_tabla(lista,title):
    with tag('div'):
        with tag('h2'):
            text(title)
        with tag('table', klass='table table-bordered'):
            with tag('tr',style='text-align:center;'):
                for key in list(lista[0].keys()):
                    line('th', str(key))
            for item in lista[0:10]:
                with tag('tr'):
                    for value in list(item.values()):
                        line('td', str(value))

# Imprime como cartas las listas pasadas por parametro
def imprimir_card(lista,title):
    with tag('div'):
        with tag('h2'):
            text(title)
        for item in lista[0:10]:
            with tag('div', klass='card'):
                if title == "Posts":
                    with tag('h5', klass='card-header'):
                        text(item['title'])
                if title == "Comments":
                    with tag('h5', klass='card-header'):
                        text(item['email'])
                with tag('div', klass='card-body'):
                    with tag('p', klass='card-text'):
                        text(item["body"])
            with tag('br'):
                pass


# Realizamos las peticiones para traer los datos de posts, comments etc....
posts = json.load(request.urlopen("https://jsonplaceholder.typicode.com/posts/"))
comments = json.load(request.urlopen("https://jsonplaceholder.typicode.com/comments/"))
photos = json.load(request.urlopen("https://jsonplaceholder.typicode.com/photos/"))
todos = json.load(request.urlopen("https://jsonplaceholder.typicode.com/todos/"))
users = json.load(request.urlopen("https://jsonplaceholder.typicode.com/users/"))

# Instanciamos el objeto Doc de yattag
doc, tag, text, line = Doc().ttl()

# Documento html
doc.asis('<!DOCTYPE html>')
with tag('html', lang="en"):
    with tag('head'):
        doc.asis('<meta charset="utf-8">')
        doc.asis('<meta name="viewport" content="width=device-width, initial-scale=1">')
        doc.asis('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">')
        with tag('script', src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"):
            pass
        with tag('script', src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"):
            pass
        with tag('body', klass='container'):
            with tag('div'):
                with tag('center'):
                    with tag('h1'):
                        text('LSV TEAM')
                with tag('h2'):
                    text('Fotos')
                with tag('div',klass='row'):
                    for photo in photos[0:10]:
                        with tag('div', klass='col-md-3'):
                            with tag('div', klass='card', style='width: 18rem;'):
                                doc.stag('img', kclass='card-img-top',src=photo["thumbnailUrl"])
                                with tag('div', klass='card-body'):
                                    with tag('h5', klass='card-title'):
                                        text(photo["title"])
            imprimir_tabla(users,"Users")
            imprimir_tabla(todos,"Todos")
            imprimir_card(posts,"Posts")
            imprimir_card(comments,"Comments")

"""
    Creamos el archivo y escribimos todo el documento html
"""
f = open("index.html","w")
f.write(doc.getvalue())
f.close()



