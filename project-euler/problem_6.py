
def squares_sum():
    sum_100 = 0
    sum_10 = 0
    for i in range(1,101):
        sum_100 += i
    for j in range(1,101):
        sum_10 += j**2
    res = sum_100**2 - sum_10
    return res

print("La diferencia es de {}".format(squares_sum()))