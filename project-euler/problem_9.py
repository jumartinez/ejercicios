
from functools import reduce

def multiplicacion(x1,x2): return x1 * x2
def tripleta():
    c = 0
    for i in range(1,300):
        for j in range(1,400):
            c = (i**2) + (j**2)
            if (((c)**(1/2)) + i + j) == 1000 and (i < j < (c**(1/2))):
                return [i,j,c**(1/2)]

print("El producto de {} es".format(tripleta()), reduce(multiplicacion, tripleta()))