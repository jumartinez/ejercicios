# Even Fibonacci numbers

def even_fibonacci_sum():
    sum = 2
    a, b = 1,2
    c = 0
    while c < 4000000:
        c = a + b
        a = b
        b = c
        if c % 2 == 0:
            sum += c

    return sum
print(even_fibonacci_sum())