def largest_prime_factor(n):
    num = []
    for i in range(1, int(n / 2) + 1):
        if n % i == 0 and es_primo(i):
            num.append(i)
            if producto(num) == n:
                return num[-1]


def producto(num):
    total = 1
    for i in num:
        total *= i
    return total


def es_primo(n):
    count = 0
    for i in range(2, n):
        if n % i == 0:
            count = count + 1
            if count > 0:
                return False
    if count == 0:
        return True


print("El factor primo mas largo es:", largest_prime_factor(600851475143))