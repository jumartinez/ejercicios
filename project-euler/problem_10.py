
# Funcion para determinar si un numero es primo
def es_primo(n):
    count = 0
    x = int(n**(1/2))
    for i in range(2, x+1):
        if n % i == 0:
            count = count + 1
            if count > 0:
                return False
    if count == 0:
        return True

# Con esta funcion sumamos los numeros primos por debajo de 2000000
def summation_of_primes():
    sumatoria = 0
    i = 2
    while i < 2000000:
        if es_primo(i):
            sumatoria += i
            print(i)
        i += 1
    return sumatoria


print("La sumatoria de los numeros primos por debajo de 2000000 es {}".format(summation_of_primes()))

# 142913828922


