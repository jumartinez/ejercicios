def es_primo(n):
    count = 0
    for i in range(2, int(n/2)+1):
        if n % i == 0:
            count = count + 1
            if count > 0:
                return False
    if count == 0:
        return True

def prime_10001st():
    primos = 2
    contador = 0
    ward = True
    while(ward):
        if es_primo(primos):
            contador += 1
            if contador == 10001:
                break
            print(contador)
        primos += 1

    return "El numero primo en la posicion 10001 es {}".format(primos)

print(prime_10001st())
