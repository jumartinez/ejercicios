# Multiples of 3 and 5


def sum_multiples_3_or_5():
    sum = 0
    for i in range(1,1000):
        if i % 5 == 0 or i % 3 == 0:
            sum += i
    return sum

print("Suma multiplos de 3 o 5, abajo de 1000 :", sum_multiples_3_or_5())