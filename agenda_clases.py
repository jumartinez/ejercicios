
"""
    <Clase agenda>

    Contiene los metodos para guardar, listar, eliminar y actualizar
    contactos y un pequeño metodo estatico para mostrar un menu de la agenda
"""
class Agenda:
    listado_contactos = []

    def guardar_contacto(self,contacto):
        Agenda.listado_contactos.append(contacto)
        print("Contacto {} guardado exitosamente".format(contacto.nombre))

    def listar_contactos(self):
        for contacto in Agenda.listado_contactos:
            print(contacto.imprimir())
            print("---------------")

    def eliminar_contacto(self,id):
        size = len(Agenda.listado_contactos)
        for i in range(size):
            if Agenda.listado_contactos[i].id == id:
                return Agenda.listado_contactos.pop(i)
            else:
                return False

    def update_contact(self, contacto,**kwargs):
        contacto.imprimir()
        if kwargs["nombre"]:
            contacto.nombre = kwargs["nombre"]
        if kwargs["telefono"]:
            contacto.telefono = kwargs["telefono"]
        if kwargs["direccion"]:
            contacto.direccion = kwargs["direccion"]
        self.guardar_contacto(contacto)
        print("Contacto {} actualizado exitosamente".format(contacto.nombre))

    @classmethod
    def menu(cls):
        print("Menu de Agenda, elija una opcion")
        print("------------------")
        print("1. Listar contactos")
        print("2. Eliminar contacto")
        print("3. Actualizar contacto")
        return input("Opcion: ")

"""
    <Clase Contacto>
    
    Se define el metodo para imprimir un contacto con todos sus atributos,
    se utiliza **kwards para pasar un diccionario de algunos de sus atributos 
    a su constructor 
"""
class Contacto:

    def __init__(self,id,nombre,telefono,**kwargs):
        self.id = id
        self.nombre = nombre
        self.telefono = telefono
        self.tel_ext = kwargs["ext_tel"]
        self.direccion = kwargs["direccion"]
        self.whatsapp = kwargs["whatsapp"]
        self.facebook = kwargs["facebook"]
        self.twitter = kwargs["twitter"]

    def imprimir(self):
        return "Id: {}\nNombre: {}\nTelefono: {}\nTel. Extras: {}\nDireccion: {}" \
               "\nWhatsapp: {}\nFacebook: {}\nTwitter: {}".format(self.id,self.nombre,self.telefono,self.tel_ext,self.direccion,self.whatsapp,self.facebook,self.twitter)

# Contador de Ids
id = 1
# Instanciamos una agenda
agenda = Agenda()
# Pedimos los datos del contacto y los guardamos
while True:
    ext_tel = []
    nombre = input("Ingrese nombre contacto: ")
    telefono = input("Ingrese el telefono: ")
    extras = int(input("Ingrese el numero de tel. extras: "))
    for i in range(extras):
        telefono = input("Ingrese el telefono extra {}: ".format(i+1))
        ext_tel.append(telefono)
    direccion = input("Ingrese su direccion: ")
    whatsapp = True if input("Tiene whatsapp? (si/no): ") == "si" else False
    facebook = True if input("Tiene facebook? (si/no): ") == "si" else False
    twitter = True if input("Tiene twitter? (si/no): ") == "si" else False
    c = Contacto(str(id),nombre,telefono,ext_tel=ext_tel,direccion=direccion,whatsapp=whatsapp,facebook=facebook,twitter=twitter)
    agenda.guardar_contacto(c)
    seguir = input("Quieres seguir añadiendo contactos? (si para seguir o cualquier cosa para salir) ")
    id += 1
    if seguir.lower() != "si":
        break
    print("------------------")

# Desplegamos el menu para realizar las demas operaciones de la agenda, listar, eliminar, etc...
while True:
    op = Agenda.menu()
    if op == "1":
        agenda.listar_contactos()
    elif op == "2":
        id = input("Ingrese el id del contacto a eliminar: ")
        c = agenda.eliminar_contacto(id)
        if c:
            print("Contacto {} eliminado exitosamente".format(c.nombre))
        else:
            print("Contacto no existe")
    elif op == "3":
        id = input("Ingrese el id del contacto a actualizar: ")
        nombre = ""
        telefono = ""
        direccion = ""
        contacto = agenda.eliminar_contacto(id)
        if input("Desea actualizar nombre (si/no): ") == "si":
            nombre = input("Ingrese nombre contacto: ")
        if input("Desea actualizar telefono (si/no): ") == "si":
            telefono = input("Ingrese el telefono: ")
        if input("Desea actualizar direccion (si/no): ") == "si":
            direccion = input("Ingrese su direccion: ")
        agenda.update_contact(contacto,nombre=nombre,telefono=telefono,direccion=direccion)
    if input("Desea salir? (si/no) ") == "si":
        break