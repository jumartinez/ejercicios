
# Esta funcion pide los datos a llenar en la agenda
def llenar_agenda():
    agenda = dict()
    seguir = ""
    while True:
        update = ""
        nombre = input("Ingrese nombre contacto: ")
        if nombre in list(agenda.keys()):
            while nombre in agenda.keys() and update.lower() != "si":
                update = input("Este nombre esta en la agenda, desea actualizarlo su telefono? (si/no) ")
                if update.lower() == "si":
                    telefono = input("Ingrese el telefono: ")
                    if telefono in list(agenda.values()):
                        while telefono in agenda.values():
                            telefono = input("Telefono repetido, ingresa otro telefono: ")
                    agenda[nombre] = telefono
                    print("Contacto actualizado!")
                else:
                    nombre = input("Man ingresa otro nombre: ")
        if update.lower() != "si":
            telefono = input("Ingrese el telefono: ")
            if telefono in list(agenda.values()):
                while telefono in agenda.values():
                    telefono = input("Telefono repetido, ingresa otro telefono: ")
            agenda[nombre] = telefono
            print("Contacto guardado!")
        seguir = input("Quieres seguir añadiendo contactos? (si para seguir o cualquier cosa para salir) ")
        if seguir.lower() != "si":
            break
    return agenda

agenda = llenar_agenda()
print("Agenda", agenda)
print("Numero de contactos", len(agenda))
