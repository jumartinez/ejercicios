from http.server import HTTPServer, BaseHTTPRequestHandler
from servidor_estatico import rutas

class StaticServer(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/style.css':
            content = self.handle_http(200,'text/css')

            self.wfile.write(content)
        if self.path == '/' or self.path == '/about' or self.path == '/contact' or self.path == '/api':
            content = self.handle_http(200, 'text/html')
            self.wfile.write(content)
        else:
            content = self.handle_http(404, 'text/html')
            self.wfile.write(content)


    def handle_http(self,status,conten_type):
        self.send_response(status)
        self.end_headers()
        if status == 200:
            routes_content = rutas.rutas[self.path]
            f = open(routes_content, 'r')
        elif status == 404:
            f = open('error.html', 'r')
        return bytes(f.read(), 'UTF-8')

def run(server_class=HTTPServer, handler_class=StaticServer, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd on port {}'.format(port))
    httpd.serve_forever()


run()