
def ingresar_datos():
    n = int(input("Ingrese el numero de equipos: "))
    nombre = ""
    pg = 0
    pe = 0
    pp = 0
    gf = 0
    gc = 0
    equipos = []
    for i in range(n):
        nombre = input("Nombre equipo: ")
        pg = int(input("Partidos ganados: "))
        pp = int(input("Partidos perdidos: "))
        pe = int(input("Partidos empatados: "))
        p = pg*3+pe
        equipos.append({
            "nombre" : nombre,
            "puntos" : p,
            })
    return equipos

def mayor_puntos(equipos):
    for j in range(len(equipos)-1,0,-1):
        for i in range(j):
            if equipos[i]["puntos"] < equipos[i+1]["puntos"]:
                temp = equipos[i]
                equipos[i] = equipos[i+1]
                equipos[i+1] = temp

    return equipos


for i in mayor_puntos(ingresar_datos()):
    print("Nombre equipo: {} Puntos: {}".format(i["nombre"],i["puntos"]))