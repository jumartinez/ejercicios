
# Clase agencia
class AgenciaTurismo:

    def __init__(self, codigo,nombre, direccion, conductores=[], tours=[]):
        self.codigo = codigo
        self.nombre = nombre
        self.direccion = direccion
        self.conductores = []
        self.tours = []

    # Guarda los conductores de una agencia
    def guardar_conductor(self,conductor):
        if conductor not in self.conductores:
            conductor.codigo_agencia = self.codigo
            self.conductores.append(conductor)

    # Retorna el nemero de conductores de una agencia
    def total_conductores(self):
        return "El total de conductores de la agencia {} es {}".format(self.nombre,len(self.conductores))

    # Guarda los tours de una agencia
    def guardar_tours(self,tour):
        if tour not in self.tours:
            self.tours.append(tour)

    # Metodo de clase para retornar el tour de mayor duracion
    @classmethod
    def mayor_mes(cls):
        mayor = 0
        tour = Tours()
        for i in cls.tours:
            if i.duracion > mayor:
                tour = i
        return tour

    # Metodo de clase para saber el total de turistas por agencia
    @classmethod
    def total_turistas(cls):
        total_t = 0
        print(cls.tours[0].turistas[0].nombre)
        for i in cls.tours:
            total_t += len(i.turistas)
        return "El total de turistas es {}".format(str(total_t))

    # Muestra los tours por cada agencia
    def tours_agencia(self):
        for i in self.tours:
            print(i.imprimir())

    # Porcentaje de tours por conductor
    def porcentaje_tour_c(self, codigo):
        contador = 0
        for i in self.tours:
            if codigo == i.codigo_conductor:
                contador += 1
        return "{}% de tours para el conductor con id {}".format(contador/len(self.tours)*100,codigo)

# Clase tour
class Tours:

    def __init__(self, codigo, destino, fecha, costo, duracion, codigo_conductor, turistas=[]):
        self.codigo = codigo
        self.destino = destino
        self.fecha = fecha
        self.costo = costo
        self.duracion = duracion
        self.codigo_conductor = codigo_conductor
        self.turistas = []

    # Guarda el turista en un tour
    def guardar_turista(self,turista):
        if turista not in self.turistas:
            self.turistas.append(turista)

    # Reorna el total de turistas por tour
    def total_turistas(self):
        return "El total de turistas del tour {} es {}".format(self.destino,len(self.turistas))

    # Agrupa los turistas por sexo
    def turistas_por_sexo(self):
        m = list(filter(lambda  item: item.sexo == "M",self.turistas))
        f = list(filter(lambda  item: item.sexo == "F",self.turistas))
        return [m,f]

    # Agrupa los turistas por nacionalidad
    def turistas_nacionalidad(self):
        turistas = {}
        for i in self.turistas:
            if not turistas:
                turistas[i.nacionalidad] = []
            else:
                turistas[i.nacionalidad] = []

        for item in list(turistas.keys()):
            turis = []
            for j in self.turistas:
                if j.nacionalidad == item:
                    turis.append(j)
            turistas[item] = turis
        return turistas

    # Imprimir datos de un tour
    def imprimir(self):
        return "Codigo {} Destino {} Fecha {} Costo {} Duracion {}".format(self.codigo,self.destino,self.fecha,self.costo,self.duracion)

# Clase conductores
class Conductores:

    def __init__(self, id, codigo, nombre, apellido, sexo, edad, nacionalidad, codigo_agencia=""):
        self.id = id
        self.codigo = codigo
        self.nombre = nombre
        self.apellido = apellido
        self.sexo = sexo
        self.edad = edad
        self.nacionalidad = nacionalidad
        self.codigo_agencia = codigo_agencia

# Clase turistas
class Turistas:

    def __init__(self, id, nombre, apellido, nacionalidad, sexo, edad):
        self.id = id
        self.nombre = nombre
        self.apellido = apellido
        self.nacionalidad = nacionalidad
        self.sexo = sexo
        self.edad = edad

    # Imprimir turistas
    def imprimir(self):
        return "{} {}".format(self.nombre,self.apellido)

cartagena = AgenciaTurismo("1","Agencia Cartagena","Centro")
cali = AgenciaTurismo("2", "Agencia Cali","En algun lado de cali")

conductor1 = Conductores("1","DFS","Pedro","Martinez","M",17,"Colombia")
conductor2 = Conductores("2","DYU","Juan","Cabeza","F",44,"Venezuela")
conductor3 = Conductores("3","LOP","Edelmiro","Perez","F",13,"Ecuador")
conductor4 = Conductores("4","ABC","David","Martinez","M",45,"Peru")

cartagena.guardar_conductor(conductor1)
cartagena.guardar_conductor(conductor2)
cali.guardar_conductor(conductor3)
cali.guardar_conductor(conductor4)

turistas1 = Turistas("1","Juanita", "Zimanca", "Estados Unidos", "F", 49)
turistas2 = Turistas("2","Fabio", "Gomez", "China", "M", 12)
turistas3 = Turistas("3","Ramiro", "Verbel", "China", "M", 34)
turistas4 = Turistas("4","Tadeo", "Lozano", "Nigeria", "M", 44)
turistas5 = Turistas("5","Ana", "Ramirez", "Mexico", "F", 32)

tour1 = Tours("T1","Baru","12/12/2020",100000,4,conductor1.codigo)
tour1.guardar_turista(turistas1)
tour1.guardar_turista(turistas2)
tour2 = Tours("T2","Algun lado en cali","16/07/2019",50000,5,conductor2.codigo)
tour2.guardar_turista(turistas3)
tour2.guardar_turista(turistas4)
tour3 = Tours("T3","Playa blanca","13/05/2019",43000,3,conductor3.codigo)
tour3.guardar_turista(turistas5)


cali.guardar_tours(tour2)
cartagena.guardar_tours(tour1)
cartagena.guardar_tours(tour3)


m, f = tour1.turistas_por_sexo()
print("Turistas agrupados por sexo")
print("Masculinos")
for i in m:
    print(i.imprimir())
print("Femeninos")
for j in f:
    print(j.imprimir())

print("Turistas agrupados por nacionalidad")
for k,v in tour1.turistas_nacionalidad().items():
    print(k)
    for i in v:
        print(i.imprimir())

print(tour1.total_turistas())
print(tour2.total_turistas())


cali.tours_agencia()
print("------")
cartagena.tours_agencia()

print("------")
print(cartagena.porcentaje_tour_c("DFS"))


