import numpy as np
# Matriz del sudoku
sudoku = np.zeros([9,9])
sudoku[0][0] = 8
sudoku[1][2] = 3
sudoku[1][3] = 6
sudoku[2][1] = 7
sudoku[2][4] = 9
sudoku[2][6] = 2
sudoku[3][1] = 5
sudoku[3][5] = 7
sudoku[4][4] = 4
sudoku[4][5] = 5
sudoku[4][6] = 7
sudoku[5][3] = 1
sudoku[5][7] = 3
sudoku[6][2] = 1
sudoku[6][7] = 6
sudoku[6][8] = 8
sudoku[7][2] = 8
sudoku[7][3] = 5
sudoku[7][7] = 1
sudoku[8][1] = 9
sudoku[8][6] = 4