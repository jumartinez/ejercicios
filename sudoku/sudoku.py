import default_data as dd

sudoku = dd.sudoku
pos = [0,0]

# Realizamos las tres validaciones basicas del sudoku
def validaciones(n, pi=0, pj=0,r1=0,r2=0):
    global sudoku
    # Valida si el numero se repite en su cuadro
    for i in range(3):
        for j in range(3):
            if n == sudoku[i+r1][j+r2]:
                return False
    # Valida que el numero no se repita en vertical
    for i in range(0,9):
        if n == sudoku[i][pj]:
            return False
    # Valida que el numero no se repita en horizontal
    if n in sudoku[pi]:
        return False
    sudoku[pi][pj] = n
    return True

# Validacion para saber si el sudoku tiene espacios vacios
def tiene_vacios(sudoku):
    global pos
    for i in range(len(sudoku)):
        for j in range(len(sudoku)):
            if sudoku[i][j] == 0:
                pos[0] = i
                pos[1] = j
                return True
    return False

# Funcion recursiva que resuelve el sudoku
def solve_sudoku():
    global sudoku
    global l
    # Condicion base, si el sudoku no tiene espacios vacios la condicion se hace True y se detiene la recursion
    if (not tiene_vacios(sudoku)):
        return True
    # Posiciones (0,0) de la matriz del sudoku
    row = pos[0]
    col = pos[1]
    # Recorre los numeros del 1 al 9, evaluando si cumplen con las validaciones
    # Si las cumple se asigna el numero en la posicion
    for num in range(1, 10):
        if (validaciones(num,row,col, row - (row%3), col - (col%3))):
            sudoku[row][col] = num
            if (solve_sudoku()):
                return True
            sudoku[row][col] = 0
    return False

solve_sudoku()
print(sudoku)





