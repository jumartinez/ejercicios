import default_data as dd
import dtasur as dt

"""
    Aqui queria realizarlo utilizando graph coloring, pasando el sudoku a un grafo
    representandolo con su matriz de adyacencia, me tocaba modificar la clase dtasur de una 
    libreria (pyclustering) y pues lo deje hasta ahi
"""
sudoku = dd.sudoku

s = dd.np.arange(81).reshape((9,9))
ma = dd.np.zeros([81,81])

def todo(n, pi=0, pj=0,r1=0,r2=0):
    global s
    nums = []
    for i in range(3):
        for j in range(3):
            if n != s[i+r1][j+r2]:
                nums.append(s[i+r1][j+r2])
    for i in range(9):
        if n != s[i][pj]:
            nums.append(s[i][pj])
        if n != s[pi][i]:
            nums.append(s[pi][i])
    return list(set(nums))



def buscar_num(n):
    global s
    for i in range(9):
        for j in range(9):
            if n == s[i][j]:
                return [i,j]

def matrix_adyacencia():
    global ma
    p = []
    adyacentes = []
    for num in range(81):
        p = buscar_num(num)
        adyacentes = todo(num,p[0],p[1],p[0]-(p[0]-p[0]%3),p[1]-(p[1]%3))
        for anum in adyacentes:
            ma[num][anum] = 1
    return ma

data = matrix_adyacencia()
list_su = []
for i in range(len(sudoku)):
    for j in range(len(sudoku)):
        list_su.append(sudoku[i][j])


dsatur_instance = dt.dsatur(data,list_su)
dsatur_instance.process()
colors = dsatur_instance.get_colors()
print(colors)