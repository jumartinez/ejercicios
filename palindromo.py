def is_palindromo(msg):
    a = []
    b = []
    c = ""
    d = ""
    vocales = {
        "á":"a",
        "é":"e",
        "í":"i",
        "ó":"o",
        "ú":"u"
    }
    msg = msg.replace(" ","").lower()
    for i in msg:
        if i in vocales:
            a.append(vocales[i])
        else:
            a.append(i.lower())
    for j in range(len(msg)-1,-1,-1):
        if msg[j] in vocales:
            b.append(vocales[msg[j]])
        else:
            b.append(msg[j])
    c = "".join(a)
    d = "".join(b)
    print(c)
    print(d)
    if c == d:
        return True
    else:
        acu = 0
        for z in range(len(msg)):
            if c[z] == d[z]:
                acu += 1
        if round(acu/len(msg),2)*100 >= 80:
            return "Casi palindroma "+str(round(acu/len(msg),2)*100)+"%"
        else:
            return False

print(is_palindromo("sometamos y matemos"))